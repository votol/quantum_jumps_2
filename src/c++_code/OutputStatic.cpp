#include "Output.h"

class OutputTime:public IOutput
{
	std::string name;
	std::vector<double> data;
	std::vector<double>::iterator current_it;
	std::vector<size_t> Dims;
public:
	OutputTime(const unsigned int& num )
	{
		data.resize(num,0.0);
		current_it=data.begin();
		name = "Time";
		Dims.push_back(1);
	}
	virtual ~OutputTime(){};
	virtual const std::string& GetName() override
		{
		return name;
		}
	virtual const std::vector<double>& GetData() override
		{
		return data;
		}
	virtual void proceed(const cuDoubleComplex* vec, const std::vector<double> & par) override
		{
		(*current_it) = par[0];
		++current_it;
		if(current_it == data.end())
			current_it = data.begin();
		}
	virtual const std::vector<size_t>& GetDimensions() override
		{
		return Dims;
		}
};



OutputHolder::OutputHolder(std::vector<std::unique_ptr<IOperator> >& opers, const unsigned int& num)
{
	outputs.resize(1);
	outputs[0] = std::unique_ptr<IOutput>(new OutputTime(num));
	init(opers,num);
}
