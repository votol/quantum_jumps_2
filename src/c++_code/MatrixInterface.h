#ifndef __MATRIXINTERFACE_H_
#define __MATRIXINTERFACE_H_
#include <cuComplex.h>
#include <vector>
#include <memory>


class IMatrix
{
public:
	IMatrix(){};
	virtual ~IMatrix(){};
	virtual void apply(cuDoubleComplex*, const cuDoubleComplex*,
			const cuDoubleComplex&, const cuDoubleComplex&) = 0;
};

class IMatrixContainer
{
public:
	IMatrixContainer(){};
	virtual ~IMatrixContainer(){};
	virtual std::vector<std::unique_ptr<IMatrix> > & GetMatrices(void) = 0;
};


#endif /* __MATRIXINTERFACE_H_ */
