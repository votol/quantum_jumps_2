#ifndef __MATRIXCONTAINERFABRIC_H_
#define __MATRIXCONTAINERFABRIC_H_
#include "MatrixInterface.h"
#include <fstream>
#include <memory>

class MatrixContainerFabric
{
public:
	MatrixContainerFabric(){};
	~MatrixContainerFabric(){};
	std::shared_ptr<IMatrixContainer> GetInstance(std::ifstream&);
};


#endif /* __MATRIXCONTAINERFABRIC_H_ */
