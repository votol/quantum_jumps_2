#include "cuMatrix.h"
#include "dimension_define.h"
#include "MatrixContainerFabric.h"
#include <algorithm>
#include <iostream>

struct COOelement
{
	int col;
	int row;
	cuDoubleComplex val;
	bool operator<(const COOelement& in) const
	{
		if(row == in.row)
		{
			if(col < in.col)
				return true;
		}
		else if(row < in.row)
			return true;
		return false;
	}
};



CSRMatrix::CSRMatrix(std::ifstream& is)
{
	int nnz;
	is.read((char*)&nnz,4);
	std::vector<COOelement> coo_matrix(nnz);
	for(auto it=coo_matrix.begin();it!=coo_matrix.end();++it)
	{
		is.read((char *)&(it->row), 4);
	}
	for(auto it=coo_matrix.begin();it!=coo_matrix.end();++it)
	{
		is.read((char *)&(it->col), 4);
	}
	for(auto it=coo_matrix.begin();it!=coo_matrix.end();++it)
	{
		is.read((char *)&(it->val.x), 8);
		is.read((char *)&(it->val.y), 8);
	}
	std::sort(coo_matrix.begin(), coo_matrix.end());
	row.resize(DIM+1);
	col.resize(nnz);
	val.resize(nnz);
	int row_id = 0;
	int index = 0;
	auto col_it = col.begin();
	auto val_it = val.begin();
	row[0] = 0;
	for(auto it = coo_matrix.begin();it != coo_matrix.end();++it)
	{
		while(row_id != it->row)
		{
			++row_id;
			row[row_id] = index;
		}
		*col_it = it->col;
		*val_it = it->val;
		++col_it;
		++val_it;
		++index;
	}
	++row_id;
	while(row_id != DIM+1)
	{
		row[row_id] = nnz;
		++row_id;
	}
}

int CSRMatrix::GetNumberOfNonZeroElements() const
{
	return val.size();
}

const int* CSRMatrix::GetCol() const
{
	return col.data();
}

const int* CSRMatrix::GetRow() const
{
	return row.data();
}

const cuDoubleComplex* CSRMatrix::GetVal() const
{
	return val.data();
}

cudaMatrixContainer::cudaMatrix::cudaMatrix(const CSRMatrix& in)
{
	nnz = in.GetNumberOfNonZeroElements();
	cudaMalloc((void**)&cuda_col,nnz * sizeof(cuda_col[0]));
	cudaMalloc((void**)&cuda_val,nnz * sizeof(cuda_val[0]));
	cudaMalloc((void**)&cuda_row,(DIM + 1) * sizeof(cuda_row[0]));
	cudaMemcpy(cuda_col, in.GetCol(), (size_t)nnz * sizeof(cuda_col[0]),
			cudaMemcpyHostToDevice);
	cudaMemcpy(cuda_row, in.GetRow(), (size_t)(DIM+1) * sizeof(cuda_row[0]),
				cudaMemcpyHostToDevice);
	cudaMemcpy(cuda_val, in.GetVal(), (size_t)nnz * sizeof(cuda_val[0]),
				cudaMemcpyHostToDevice);
}

cudaMatrixContainer::cudaMatrix::~cudaMatrix()
{
	cudaFree(cuda_col);
	cudaFree(cuda_val);
	cudaFree(cuda_row);
}

void cudaMatrixContainer::cudaMatrix::apply(cuDoubleComplex* out,
		const cuDoubleComplex* in,
		const cuDoubleComplex& alpha,const cuDoubleComplex& beta)
{
	cusparseZcsrmv(handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
			DIM, DIM, nnz, &alpha, descr, cuda_val,
			cuda_row, cuda_col, in, &beta, out);
}

cusparseHandle_t cudaMatrixContainer::cudaMatrix::handle = 0;
cusparseMatDescr_t cudaMatrixContainer::cudaMatrix::descr = 0;

cudaMatrixContainer::cudaMatrixContainer(std::ifstream& is)
{
	cusparseCreate(& (cudaMatrix::handle));
	cusparseCreateMatDescr(& (cudaMatrix::descr));
	cusparseSetMatType(cudaMatrix::descr,CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(cudaMatrix::descr,CUSPARSE_INDEX_BASE_ZERO);
	int number_of_matrices;
	is.read((char*)&number_of_matrices, 4);
	matrices.resize(number_of_matrices);
	for(auto it = matrices.begin(); it != matrices.end(); ++it)
		*it = std::unique_ptr<IMatrix>(new cudaMatrix(CSRMatrix(is)));
}

cudaMatrixContainer::~cudaMatrixContainer()
{
	cusparseDestroyMatDescr(cudaMatrix::descr);
	cusparseDestroy(cudaMatrix::handle);
}

std::vector<std::unique_ptr<IMatrix> > & cudaMatrixContainer::GetMatrices(void)
{
	return matrices;
}

std::shared_ptr<IMatrixContainer> MatrixContainerFabric::GetInstance(std::ifstream& is)
{
	return std::shared_ptr<IMatrixContainer>(new cudaMatrixContainer(is));
}

