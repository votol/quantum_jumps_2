#ifndef __Output_h_
#define __Output_h_
#include "OutputInterface.h"
#include "OperatorInterface.h"
#include <memory>

class OutputHolder
{
	std::vector<std::unique_ptr<IOutput> > outputs;

	void init(std::vector<std::unique_ptr<IOperator> >&,const unsigned int&);
public:
	OutputHolder(std::vector<std::unique_ptr<IOperator> >&,const unsigned int&);
	~OutputHolder(){};
	std::vector<std::unique_ptr<IOutput> >& GetOutputs(void){return outputs;};
};


#endif /*__Output_h_*/
