#include "InitStateGeneral.h"
#include "InitState.h"
#include "dimension_define.h"

bool InitStateCompound::increment()
{
	for(auto it = iterators.begin();it != iterators.end();++it)
	{
		it->current_it++;
		if(it->current_it == it->end_it)
			it->current_it = it->begin_it;
		else
			return true;
	}
	return false;
}

void InitStateCompound::GetState(std::vector<complex<double> >& st)
{
	st.resize(DIM);
	iterators.resize(states.size());

	auto iterators_it = iterators.begin();
	for(auto it=states.begin();it!=states.end();++it)
	{
		iterators_it->begin_it=it->begin();
		iterators_it->current_it=it->begin();
		iterators_it->end_it=it->end();
		++iterators_it;
	}

	auto st_it = st.begin();
	do
	{
		*st_it = complex<double>(1.0,0.0);
		for(auto it = iterators.begin();it != iterators.end();++it)
			*st_it *= *(it->current_it);
		++st_it;
	}while(increment());
}


GeneralInitState::GeneralInitState(const std::vector<double>& pr)
{
	SetCompounds(pr);
	state.resize(DIM, complex<double>(0.0));
	std::vector<complex<double> > tmp;
	for(auto it = compounds.begin();it != compounds.end(); ++it)
	{
		it->state->GetState(tmp);
		auto it1 = state.begin();
		for(auto it2 = tmp.begin(); it2 != tmp.end(); ++it2)
		{
			(*it1) += it->weight * (*it2);
			++it1;
		}
	}
	double norm = 0.0;
	for(auto it =state.begin();it!=state.end();++it)
	{
		norm += abs2(*it);
	}

	norm = sqrt(norm);

	for(auto it =state.begin();it!=state.end();++it)
	{
		*it /= norm;
	}
}

std::vector<complex<double> >& GeneralInitState::GetState()
{
	return state;
}

InitStateHolder::InitStateHolder(const std::vector<double>& pr)
{
	GeneralInitState GeneralInitStateInstatnse(pr);
	std::vector<complex<double> >& state = GeneralInitStateInstatnse.GetState();
	init_state.resize(state.size());
	auto it = state.begin();
	for(auto it1 = init_state.begin();it1!=init_state.end();++it1)
	{
		*it1 = make_cuDoubleComplex(it->__re,it->__im);
		++it;
	}
}

