#ifndef __cuMatrix_h_
#define __cuMatrix_h_
#include <cuda_runtime.h>
#include <cusparse_v2.h>
#include <iostream>
#include <fstream>
#include "MatrixInterface.h"


class CSRMatrix
{
	std::vector<cuDoubleComplex> val;
	std::vector<int> col;
	std::vector<int> row;
public:
	CSRMatrix(std::ifstream&);
	~CSRMatrix(){};
	int GetNumberOfNonZeroElements() const;
	const cuDoubleComplex* GetVal(void) const;
	const int* GetCol() const;
	const int* GetRow() const;
};

class cudaMatrixContainer: public IMatrixContainer
{
	class cudaMatrix:public IMatrix
	{
		friend cudaMatrixContainer;
		static cusparseHandle_t handle;
		static cusparseMatDescr_t descr;
		int nnz;
		int* cuda_col;
		int* cuda_row;
		cuDoubleComplex* cuda_val;
	public:
		cudaMatrix(const CSRMatrix& );
		~cudaMatrix();
		virtual void apply(cuDoubleComplex* out, const cuDoubleComplex* in,
				const cuDoubleComplex& alpha, const cuDoubleComplex& beta);
	};

std::vector<std::unique_ptr<IMatrix> > matrices;

public:
	cudaMatrixContainer(std::ifstream&);
	virtual ~cudaMatrixContainer();
	virtual std::vector<std::unique_ptr<IMatrix> > & GetMatrices(void);
};

#endif /*__cuMatrix_h_*/
