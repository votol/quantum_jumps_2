#ifndef __InitStateGeneral_h_
#define __InitStateGeneral_h_
#include <vector>
#include <list>
#include <memory>
#include "complex.h"

class InitStateCompound
{

	using Iterator = std::vector<complex<double> >::iterator;
	struct Iterators
	{
		Iterator begin_it;
		Iterator end_it;
		Iterator current_it;
	};

	std::vector<Iterators> iterators;

	bool increment();

protected:
	std::list<std::vector<complex<double> > > states;
public:
	InitStateCompound(){};
	~InitStateCompound(){};
	void GetState(std::vector<complex<double> >&);
};


class GeneralInitState
{
	struct Compound
	{
		complex<double> weight;
		std::unique_ptr<InitStateCompound> state;
	};
	std::vector<complex<double> > state;
	std::list<Compound> compounds;

	void SetCompounds(const std::vector<double>&);
public:
	GeneralInitState(const std::vector<double>&);
	~GeneralInitState(){};
	std::vector<complex<double> >& GetState();
};

#endif /*__InitStateGeneral_h_*/
