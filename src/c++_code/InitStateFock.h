#ifndef __InitStateFock_h_
#define __InitStateFock_h_
#include <vector>
#include "complex.h"

class FockInit
{
	unsigned int dim;
	unsigned int num;
public:
	FockInit(unsigned int d, const complex<double> & par):dim(d){num=int(par.__re);};
	~FockInit(){};
	std::vector<complex<double> > GetState()
		{
		std::vector<complex<double> > tmp(dim);
		tmp[num] = 1.0;
		return tmp;
		}
};


#endif /*__InitStateFock_h_*/
