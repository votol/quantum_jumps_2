#ifndef __QJumps_h_
#define __QJumps_h_
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <vector>
#include <memory>
#include <chrono>
#include <string>
#include <nvml.h>
#include "OperatorInterface.h"
#include "OutputInterface.h"


class QJumps
{
	double dt;
	unsigned int Ntr;
	unsigned int Nsteps;
	unsigned int Noutputs;
	unsigned int Nrelaxations;

	unsigned int CudaDeviceNumber;
	cublasHandle_t blas_handle;

	std::vector<double>* parameters;
	std::vector<std::unique_ptr<IOperator> >* operators;
	std::vector<cuDoubleComplex>* init_state;
	std::vector<std::unique_ptr<IOutput> >* outputs;

	cuDoubleComplex* vectors;

	std::vector<double> probabilities;
	double output_coe;
	std::chrono::high_resolution_clock::time_point t2;
	std::chrono::high_resolution_clock::time_point t1;

	nvmlDevice_t device;

	int Get_temperature(void);
	int interruption(void);

	void one_trajectory(void);
public:
	QJumps();
	~QJumps();
	void Calculate(void);
	void SetTimeStep(const double&);
	void SetTrajectoryNumber(const unsigned int&);
	void SetStepsNumber(const unsigned int&);
	void SetOutputSteps(const unsigned int&);
	void SetNumberOfRelaxations(const unsigned int&);
	void SetCudaDeviceNumber(const unsigned int&);
	void SetParameters(std::vector<double>&);
	void SetOperators(std::vector<std::unique_ptr<IOperator> >&);
	void SetInitState(std::vector<cuDoubleComplex>&);
	void SetOutputs(std::vector<std::unique_ptr<IOutput> > &);
};


#endif /*__QJumps_h_*/
