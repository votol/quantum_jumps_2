#ifndef __OutputOperator_h_
#define __OutputOperator_h_
#include "OutputInterface.h"
#include "OperatorInterface.h"
#include <memory>
#include <cublas_v2.h>

class OutputOperatorFabric;

class OutputOperator:public IOutput
{
	friend OutputOperatorFabric;
	static cuDoubleComplex* tmp_vec;
	static cublasHandle_t blas_handle;
	std::string name;
	std::unique_ptr<IOperator>& operator_instance;
	std::vector<double> data;
	std::vector<double>::iterator current_it;
	std::vector<size_t> Dims;
public:
	OutputOperator(const std::string&, std::unique_ptr<IOperator>&, const unsigned int& );
	virtual ~OutputOperator(){};
	virtual const std::string& GetName() override;
	virtual const std::vector<double>& GetData() override;
	virtual void proceed(const cuDoubleComplex* , const std::vector<double> &) override;
	virtual const std::vector<size_t>& GetDimensions() override;
};

class OutputOperatorFabric
{
public:
	~OutputOperatorFabric();
	std::unique_ptr<IOutput> GetInstance(const std::string&,
			std::unique_ptr<IOperator>&, const unsigned int&) const;
	static const OutputOperatorFabric& Fabric();
private:
	OutputOperatorFabric();
	OutputOperatorFabric(const OutputOperatorFabric& root);
	OutputOperatorFabric& operator=(const OutputOperatorFabric&);
};


#endif /*__OutputOperator_h_*/
