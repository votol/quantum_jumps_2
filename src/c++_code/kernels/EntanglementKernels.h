#ifndef __EntanglementKernels_h_
#define __EntanglementKernels_h_
#include "cuComplex.h"

extern void MakeDensityMatrix(double *, const cuDoubleComplex*,
		const unsigned int&, const unsigned int&, const unsigned int&,
		const unsigned int&, const unsigned int&);

#endif /*__EntanglementKernels_h_*/
