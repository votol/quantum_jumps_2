#include "EntanglementKernels.h"
#include "math.h"

__global__ void diagonal(double * matr,const cuDoubleComplex * vec,
		const unsigned int a, const unsigned int b, const unsigned int c,
		const unsigned int d, const unsigned int e)
{
	unsigned int id = blockIdx.x*64+threadIdx.x;
	if(id < b*d)
	{
		unsigned int m1;
		unsigned int m2;
		unsigned int L;
		unsigned int coe1 = a*b*c*d;
		unsigned int coe2 = a*b*c;
		unsigned int coe3 = a*b;
		m2 = id/b;
		m1 = id%b;
		for(unsigned int k=0;k<e;++k)
		{
			for(unsigned int l=0;l<c;++l)
			{
				for(unsigned int q=0;q<a;++q)
				{
					L = coe1 * k +coe2 * m2 +coe3 *l + a *m1 + q;
					matr[id] += vec[L].x * vec[L].x + vec[L].y * vec[L].y; 
				}
			}
		}
	}
}

__global__ void non_diagonal(double * matr,const cuDoubleComplex * vec,
		const unsigned int a, const unsigned int b, const unsigned int c,
		const unsigned int d, const unsigned int e)
{
	unsigned int id = blockIdx.x*64+threadIdx.x;
	unsigned int DIM = b*d;
	if(id < (DIM*DIM-DIM)/2)
	{
		unsigned int m;
		unsigned int m1;
		unsigned int m2;
		unsigned int n;
		unsigned int n1;
		unsigned int n2;
		unsigned int L;
		unsigned int K;
		unsigned int coe1 = a*b*c*d;
		unsigned int coe2 = a*b*c;
		unsigned int coe3 = a*b;
		m = int(double(DIM) - 0.5 - sqrt((double(DIM)-0.5)*(double(DIM)-0.5) - 2 * double(id)));
		n = id - (2*DIM-1-m)*m/2 + 1 + m;
		m2 = m/b;
		m1 = m%b;
		n2 = n/b;
		n1 = n%b;
		for(unsigned int k=0;k<e;++k)
		{
			for(unsigned int l=0;l<c;++l)
			{
				for(unsigned int q=0;q<a;++q)
				{
					L = coe1 * k +coe2 * m2 +coe3 *l + a *m1 + q;
					K = coe1 * k +coe2 * n2 +coe3 *l + a *n1 + q;
					matr[2*id] += vec[L].x * vec[K].x + vec[L].y * vec[K].y;
					matr[2*id + 1] += vec[L].x * vec[K].y - vec[L].y * vec[K].x; 
				}
			}
		}
	}
}

void MakeDensityMatrix(double * matrix, const cuDoubleComplex* vec, 
		const unsigned int& a, const unsigned int& b, const unsigned int& c,
		const unsigned int& d, const unsigned int& e)
{
	unsigned int matr_dim = b*d;
	unsigned int block_number = matr_dim/64 + 1; 	
	diagonal<<<block_number,64>>>(matrix,vec,a,b,c,d,e);
	block_number = ((matr_dim*matr_dim-matr_dim)/2)/64 +1;
	non_diagonal<<<block_number,64>>>(matrix + matr_dim,vec,a,b,c,d,e);
}