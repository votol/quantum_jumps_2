#ifndef __InitState_h_
#define __InitState_h_
#include "cuComplex.h"
#include <vector>

class InitStateHolder
{
	std::vector<cuDoubleComplex> init_state;
public:
	InitStateHolder(const std::vector<double>& );
	~InitStateHolder(){};
	std::vector<cuDoubleComplex>& GetInitState(){return init_state;};
};


#endif /*__InitState_h_*/
