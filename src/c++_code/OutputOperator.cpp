#include "OutputOperator.h"
#include "dimension_define.h"
#include <cuda_runtime.h>

OutputOperator::OutputOperator(const std::string& n,
		std::unique_ptr<IOperator>& op, const unsigned int& num):name(n),operator_instance(op)
{
	data.resize(num,0.0);
	current_it=data.begin();
	Dims.push_back(1);
}
const std::string& OutputOperator::GetName()
{
	return name;
}
const std::vector<double>& OutputOperator::GetData()
{
	return data;
}
void OutputOperator::proceed(const cuDoubleComplex* vec, const std::vector<double> &param)
{
	cuDoubleComplex tmp;
	operator_instance->apply(tmp_vec,vec,param);
	cublasZdotc(blas_handle,DIM,vec,1,tmp_vec,1,&tmp);
	(*current_it) += tmp.x;
	++current_it;
	if(current_it == data.end())
		current_it = data.begin();
}

const std::vector<size_t>& OutputOperator::GetDimensions()
{
	return Dims;
}

cuDoubleComplex* OutputOperator::tmp_vec = 0;
cublasHandle_t OutputOperator::blas_handle = 0;

OutputOperatorFabric::~OutputOperatorFabric()
{
	cublasDestroy(OutputOperator::blas_handle);
	cudaFree(OutputOperator::tmp_vec);
}

std::unique_ptr<IOutput> OutputOperatorFabric::GetInstance(const std::string& n,
		std::unique_ptr<IOperator>& op, const unsigned int& Nout)const
{
	return std::unique_ptr<IOutput>(new OutputOperator(n,op,Nout));
}


const OutputOperatorFabric& OutputOperatorFabric::Fabric()
{
	static OutputOperatorFabric theSingleInstance;
	return theSingleInstance;
}

OutputOperatorFabric::OutputOperatorFabric()
{
	cublasCreate(&(OutputOperator::blas_handle));
	cudaMalloc((void**) &(OutputOperator::tmp_vec),DIM*sizeof(OutputOperator::tmp_vec[0]));
}


