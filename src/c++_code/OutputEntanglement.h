#ifndef __OutputEntanglement_h_
#define __OutputEntanglement_h_
#include "OutputInterface.h"

class OutputEntanglement:public IOutput
{
	std::string name;
	unsigned int time_steps;
	unsigned int left;
	unsigned int first;
	unsigned int mid;
	unsigned int second;
	unsigned int right;
	std::vector<double> data;
	double * device_matr;
	double * host_matr;
	unsigned int current_step;
	std::vector<size_t> Dims;

public:
	OutputEntanglement(const std::string&, const unsigned int&,
			const unsigned int&, const unsigned int&, const unsigned int&,
			const unsigned int&, const unsigned int&);
	virtual ~OutputEntanglement();
	virtual const std::string& GetName() override;
	virtual const std::vector<double>& GetData() override;
	virtual void proceed(const cuDoubleComplex* , const std::vector<double> &) override;
	virtual const std::vector<size_t>& GetDimensions()  override;
};

#endif /*__OutputEntanglement_h_*/
