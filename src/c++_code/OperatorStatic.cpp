#include "Operator.h"

OperatorContainer::OperatorContainer(std::ifstream& is)
{
	matrix_container = MatrixContainerFabric().GetInstance(is);
	operators_setter();
}

unsigned int& OperatorContainer::GetRelaxationNumber()
{
	return relaxation_number;
}

std::vector<std::unique_ptr<IOperator> >& OperatorContainer::GetOperators(void)
{
	return operators;
}
