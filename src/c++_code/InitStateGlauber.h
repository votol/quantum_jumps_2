#ifndef __InitStateGlauber_h_
#define __InitStateGlauber_h_
#include <vector>
#include "complex.h"
#include <iostream>

class GlauberInit
{
	unsigned int dim;
	complex<double> parameter;
public:
	GlauberInit(unsigned int d, const complex<double> & par):dim(d),parameter(par){};
	~GlauberInit(){};
	std::vector<complex<double> > GetState()
	{
		std::vector<complex<double> > tmp(dim);
		double coe = exp(-0.5*abs2(parameter));
		complex<double> perd=1.0;
		for(unsigned int peri=0; peri<dim;++peri)
		{
			tmp[peri] = coe*perd;
			perd*=parameter/sqrt(double(peri+1));
		}
		double norm = 0.0;
		for(auto it = tmp.begin();it!=tmp.end();++it)
			norm += abs2(*it);
		std::cout<<"Glauber state: parameter = "<< parameter <<";norm=" << sqrt(norm) << std::endl;
		return tmp;
	}
};

#endif /*__InitStateGlauber_h_*/
