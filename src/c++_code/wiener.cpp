#include <random>
#include <math.h>
#include <exception>
#include <map>
#include "wiener.h"

class wiener_functor
{
	double current_t;
	double current_value;
	std::random_device rd;
	std::normal_distribution<double> distribution;
public:
	wiener_functor():current_t(0.0),current_value(0.0),distribution(0.0,1.0){};
	~wiener_functor(){};
	double operator() (const double& t)
	{
		if(t < current_t)
			throw std::runtime_error("Sorry current wiener process implementation can only return numbers for future.");
		if(current_t < t)
		{
			current_value += sqrt(t - current_t) * distribution(rd);
			current_t = t;
		}
		return current_value;
	}
};

std::map<int,wiener_functor> process_holder;

void clear_wiener(void)
{
	process_holder.clear();
}

double wiener(const int& in,const double& in1)
{
	return process_holder[in](in1);
}
