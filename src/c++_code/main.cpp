#include "yaml-cpp/yaml.h"
#include "Operator.h"
#include "ParametersInit.h"
#include "InitState.h"
#include "Output.h"
#include "QJumps.h"
#include "NetCdfWriter.h"
#include <cuda_runtime.h>
#include <string>
#include <exception>
#include <iostream>

int main(int argc, char **argv)
{
	YAML::Node config = YAML::LoadFile(argv[1]);
	std::string work_dir = config["paths"]
								  ["work_directory"].as<std::string>();
	std::string output_dir = config["paths"]
								  ["output_directory"].as<std::string>();
	cudaSetDevice(config["run_options"]["cuda_device_number"].as<int>());
	std::ifstream is(work_dir+"matrices.bin",
			std::ifstream::in | std::ifstream::binary);
	if((is.rdstate() & std::ifstream::failbit ) != 0 )
		throw std::runtime_error("Can't read file with matrices data");
	OperatorContainer operator_container_inst(is);
	is.close();
	ParametersHolder parameters_holder_instance(config["parameters"]);
	InitStateHolder init_state_holder(parameters_holder_instance.GetParameters());
	OutputHolder output_holder_instance(operator_container_inst.GetOperators(),
			config["parameters"]["Nout"].as<unsigned int>());


	QJumps qjumps_instatnse;
	qjumps_instatnse.SetCudaDeviceNumber(config["run_options"]["cuda_device_number"].as<unsigned int>());
	qjumps_instatnse.SetInitState(init_state_holder.GetInitState());
	qjumps_instatnse.SetNumberOfRelaxations(operator_container_inst.GetRelaxationNumber());
	qjumps_instatnse.SetOperators(operator_container_inst.GetOperators());
	qjumps_instatnse.SetOutputs(output_holder_instance.GetOutputs());
	qjumps_instatnse.SetOutputSteps(config["parameters"]["Nout"].as<unsigned int>());
	qjumps_instatnse.SetParameters(parameters_holder_instance.GetParameters());
	qjumps_instatnse.SetStepsNumber(config["parameters"]["Nsteps"].as<unsigned int>());
	qjumps_instatnse.SetTimeStep(config["parameters"]["dt"].as<double>());
	qjumps_instatnse.SetTrajectoryNumber(config["parameters"]["Ntr"].as<unsigned int>());
	qjumps_instatnse.Calculate();

	NetCdfWriter netcdf_writer_instance(output_dir + "output.nc", output_holder_instance.GetOutputs(), config["parameters"]["Nout"].as<unsigned int>());
	return 0;
}
