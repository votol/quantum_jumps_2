#include "OutputEntanglement.h"
#include "EntanglementKernels.h"
#include <cuda_runtime.h>

OutputEntanglement::OutputEntanglement(const std::string& n, const unsigned int& t_st,
			const unsigned int& a, const unsigned int& b, const unsigned int& c,
			const unsigned int& d, const unsigned int& e):name(n),time_steps(t_st),
			left(a),first(b),mid(c),second(d),right(e)
{
	data.resize(t_st*b*d*b*d, 0.0);
	cudaMalloc((void**)&device_matr, b*d*b*d*sizeof(device_matr[0]));
	host_matr =data.data();
	current_step = 0;
	Dims.push_back(b*d*b*d);
}

OutputEntanglement::~OutputEntanglement()
{
	cudaFree(device_matr);
}

const std::string& OutputEntanglement::GetName()
{
	return name;
}
const std::vector<double>& OutputEntanglement::GetData()
{
	return data;
}

void OutputEntanglement::proceed(const cuDoubleComplex* vec, const std::vector<double> & par)
{
	cudaMemcpy(device_matr, host_matr, (size_t)first*first*second*second * sizeof(host_matr[0]),
					cudaMemcpyHostToDevice);
	MakeDensityMatrix(device_matr, vec, left,first,mid,second, right);
	cudaMemcpy(host_matr, device_matr, (size_t)first*first*second*second * sizeof(host_matr[0]),
				cudaMemcpyDeviceToHost);
	host_matr += first*first*second*second;
	current_step++;
	if(current_step == time_steps)
	{
		current_step = 0;
		host_matr = data.data();
	}
}

const std::vector<size_t>& OutputEntanglement::GetDimensions()
{
	return Dims;
}
