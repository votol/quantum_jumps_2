#ifndef __Operator_h_
#define __Operator_h_
#include "MatrixContainerFabric.h"
#include "OperatorInterface.h"

class OperatorContainer
{
	std::shared_ptr<IMatrixContainer> matrix_container;
	std::vector<std::unique_ptr<IOperator> > operators;
	unsigned int relaxation_number;
	void operators_setter();
public:
	OperatorContainer(std::ifstream&);
	~OperatorContainer(){};
	unsigned int & GetRelaxationNumber(void);
	std::vector<std::unique_ptr<IOperator> >& GetOperators(void);
};

#endif /*__Operator_h_*/
