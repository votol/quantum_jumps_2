#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <ctime>
#include <thread>
#include <fstream>
#include "QJumps.h"
#include "dimension_define.h"
#include "wiener.h"
#include <iostream>
#include <exception>

int QJumps::Get_temperature(void)
    {
	unsigned int temp;
	if(nvmlDeviceGetTemperature(device,NVML_TEMPERATURE_GPU,&temp) == NVML_SUCCESS)
		return temp;
	else
		return 100;
	}


int QJumps::interruption(void)
    {
	t2 = std::chrono::high_resolution_clock::now();
	if(std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()>1000)
        {
        int lower_temperature_limit, upper_temperature_limit;
        lower_temperature_limit = 75;
        upper_temperature_limit = 80;
        int temperature;
        temperature = Get_temperature( );
        int i = 0;
        if( temperature >= upper_temperature_limit ){

            do{
                i++;
                std::this_thread::sleep_for (std::chrono::seconds(1));
                temperature = Get_temperature();
            }while( temperature > lower_temperature_limit );
            }
        t1=t2;
        return i;
        }
    return 0;
    }


void QJumps::one_trajectory(void)
{
	boost::random::mt19937 gen;
	boost::random::uniform_01<> dis;
	gen.seed(std::time(0));
	double current_output=0.0;
	int output_number = 0;
	unsigned int peri,peri1;
	cuDoubleComplex tmp_value;
	cuDoubleComplex koe;
	koe.y=0.0;
	double probability;
	double tmp_norm;
	int operator_num;
	for(peri=0;peri<Nsteps;++peri)
	{
		if(int(current_output)>=output_number)
		{
			for(auto it = outputs->begin();it!=outputs->end();++it)
				(*it)->proceed(vectors,*parameters);
			output_number++;
		}
		probabilities[0]=1.0;
		for(peri1=0;peri1<Nrelaxations;++peri1)
		{
			(*operators)[peri1+1]->apply(vectors+DIM,vectors,*parameters);
			cublasZdotc(blas_handle,DIM,vectors+DIM,1,vectors+DIM,1,&tmp_value);
			probabilities[peri1+1]=dt*tmp_value.x;
			probabilities[0] -= probabilities[peri1+1];
		}

		operator_num = 0;
		probability = dis(gen);
		for(peri1 = 0; peri1<Nrelaxations+1;++peri1)
		{
			if(probability <= probabilities[peri1])
				break;
			else
			{
				probability-=probabilities[peri1];
				operator_num++;
			}
		}
		if(operator_num == 0)
		{
			cublasZcopy(blas_handle,DIM,vectors,1,vectors + DIM,1);
			(*operators)[0]->apply(vectors +2*DIM, vectors,*parameters);
			koe.x=dt/6.0;
			cublasZaxpy(blas_handle,DIM,&koe,vectors +2*DIM,1,vectors + DIM,1);
			cublasZcopy(blas_handle,DIM,vectors,1,vectors + 3*DIM,1);
			koe.x=dt/2.0;
			cublasZaxpy(blas_handle,DIM,&koe,vectors +2*DIM,1,vectors + 3*DIM,1);
			(*parameters)[0] += dt/2.0;
			(*operators)[0]->apply(vectors +2*DIM, vectors + 3*DIM,*parameters);
			koe.x=dt/3.0;
			cublasZaxpy(blas_handle,DIM,&koe,vectors +2*DIM,1,vectors + DIM,1);
			cublasZcopy(blas_handle,DIM,vectors,1,vectors + 3*DIM,1);
			koe.x=dt/2.0;
			cublasZaxpy(blas_handle,DIM,&koe,vectors +2*DIM,1,vectors + 3*DIM,1);
			(*operators)[0]->apply(vectors +2*DIM, vectors + 3*DIM,*parameters);
			koe.x=dt/3.0;
			cublasZaxpy(blas_handle,DIM,&koe,vectors +2*DIM,1,vectors + DIM,1);
			cublasZcopy(blas_handle,DIM,vectors,1,vectors + 3*DIM,1);
			koe.x=dt;
			cublasZaxpy(blas_handle,DIM,&koe,vectors +2*DIM,1,vectors + 3*DIM,1);
			(*parameters)[0] += dt/2.0;
			(*operators)[0]->apply(vectors +2*DIM, vectors + 3*DIM,*parameters);
			koe.x=dt/6.0;
			cublasZaxpy(blas_handle,DIM,&koe,vectors +2*DIM,1,vectors + DIM,1);
		}
		else
		{
			(*operators)[operator_num]->apply(vectors+DIM,vectors,*parameters);
			(*parameters)[0] += dt;
		}
		cublasDznrm2(blas_handle,DIM,vectors+DIM,1,&tmp_norm);
		tmp_norm=1.0/tmp_norm;
		cublasZcopy(blas_handle,DIM,vectors+DIM,1,vectors,1);
		cublasZdscal(blas_handle,DIM,&tmp_norm,vectors,1);
		current_output += output_coe;
		interruption();
	}
}

QJumps::QJumps():dt(0.0),Ntr(0),Nsteps(0),Noutputs(0),Nrelaxations(0),CudaDeviceNumber(0),
				parameters(NULL), operators(NULL), init_state(NULL),outputs(NULL),probabilities(0)
				,output_coe(0.0),device(NULL)
{
	nvmlReturn_t result = nvmlInit();
	if (result != NVML_SUCCESS)
	{
		throw std::runtime_error("Failed to get handle for device " + std::to_string(CudaDeviceNumber) + ": " + std::string(nvmlErrorString(result)));
	}
	t1 = std::chrono::high_resolution_clock::now();
	t2=t1;
	cudaMalloc((void**)&vectors, 4*DIM*sizeof(vectors[0]));
	cublasCreate(&blas_handle);
}
QJumps::~QJumps()
{
	nvmlShutdown();
	cublasDestroy(blas_handle);
	cudaFree(vectors);
}
void QJumps::Calculate(void)
{
	nvmlReturn_t result = nvmlDeviceGetHandleByIndex(CudaDeviceNumber, &device);
	if (result != NVML_SUCCESS)
	{
		throw std::runtime_error("Failed to get handle for device " + std::to_string(CudaDeviceNumber) + ": " + std::string(nvmlErrorString(result)));
	}
	probabilities.resize(Nrelaxations+1);
	output_coe = double(Noutputs)/double(Nsteps);
	for(unsigned int peri = 0;peri<Ntr;++peri)
	{
		(*parameters)[0] =0.0;
		cudaMemcpy(vectors, init_state->data(),DIM*sizeof(vectors[0]),cudaMemcpyHostToDevice);
		clear_wiener();
		one_trajectory();
	}
}
void QJumps::SetTimeStep(const double& in)
{
	dt=in;
}
void QJumps::SetTrajectoryNumber(const unsigned int& in)
{
	Ntr = in;
}
void QJumps::SetStepsNumber(const unsigned int& in)
{
	Nsteps = in;
}
void QJumps::SetOutputSteps(const unsigned int& in)
{
	Noutputs = in;
}
void QJumps::SetNumberOfRelaxations(const unsigned int& in)
{
	Nrelaxations = in;
}
void QJumps::SetCudaDeviceNumber(const unsigned int& in)
{
	CudaDeviceNumber = in;
}
void QJumps::SetParameters(std::vector<double>& in)
{
	parameters = &in;
}
void QJumps::SetOperators(std::vector<std::unique_ptr<IOperator> >& in)
{
	operators = &in;
}
void QJumps::SetInitState(std::vector<cuDoubleComplex>& in)
{
	init_state = &in;
}
void QJumps::SetOutputs(std::vector<std::unique_ptr<IOutput> > & in)
{
	outputs = &in;
}

