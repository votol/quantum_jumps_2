#!/usr/bin/env python3
import ccs

calc = ccs.Calculator()

param1 = [{'Nout' : 200, 'kapa' : 0.1, 'D' : 0.0, 'Gpump' : 1.0, 'Gtwo' : 0.9, 'nT': 0.0,
               'dt' : 0.002, 'Nsteps' : 60000, 'Ntr' : 4000}]
#param2 = [{'alpha' : 0.0}, {'alpha' : 2.0}, {'alpha' : 3.0}, {'alpha' : 4.0}]
#param2 = [{'alpha' : 1.0},{'alpha' : 1.4},{'alpha' : 1.8},{'alpha' : 2.2},{'alpha' : 2.6},{'alpha' : 3.0}]
param2 = [{'alpha': 1.0}]
#param3 = []
#for ind in range(20):
#    param3.append({'g': float(ind + 1) * 3.0 / 20.0, 'dt': 0.001 - float(ind)*0.0009/19.0})
calc.add([param1,param2])