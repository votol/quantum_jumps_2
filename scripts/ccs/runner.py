#!/usr/bin/env python3

import time
import subprocess
import os

work_folder = os.getenv("HOME") + '/.runner/'

proc = subprocess.Popen(['echo'])
proc.wait()
log_file = open(work_folder + 'log','a')

while True:

    if proc.poll() is not None:
        tasks = os.listdir(work_folder + 'tasks')
        if tasks:
            if os.path.isfile(work_folder + 'tasks/' + tasks[0] + '/ready'):
                run_script_file = open(work_folder + 'tasks/' + tasks[0] + '/run_script', 'r')
                run_script = run_script_file.readlines()[0]
                run_script_file.close()
                log_file.write(time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime()) + ': ' + 'start task ' +
                               run_script + ' for ' + tasks[0] + '\n')
                log_file.flush()
                proc = subprocess.Popen([run_script, work_folder + 'tasks/' + tasks[0]], stdout = log_file,
                                        stderr = log_file)

    time.sleep(60)