#! /usr/bin/env python3

import numpy
import os
import shutil
import yaml
import copy
import threading
import uuid
import argparse


class Node:
    def __init__(self, cpu_for_node = 8):
        self.config = dict()
        self.config['run_options'] = dict()
        self.config['run_options']['threads_number'] = cpu_for_node
        self.config['run_options']['cuda_devices'] = [0,1,4,5,6,7]
        self.root_directory = os.path.realpath(__file__)
        self.root_directory = self.root_directory[:self.root_directory.rindex('/')] + '/'
        self.config['paths'] = dict()
        self.config['paths']['work_directory'] = self.root_directory + 'bin/'
    def calculate(self, parameters, col, my_id, thread_num):
        for peri in range(my_id, col, thread_num):
            for element in os.listdir(self.config['paths']['work_directory']):
                if element != 'calculator' and element != 'config.yml' and element != 'run'\
                        and element != 'output_tmp.nc' and element != 'parameters_config.yml':
                    if os.path.isfile(self.config['paths']['work_directory'] + element):
                        os.remove(self.config['paths']['work_directory'] + element)
                    else:
                        shutil.rmtree(self.config['paths']['work_directory'] + element)
            self.config['paths']['output_directory'] = parameters[peri]['dir']
            self.config['parameters'] = parameters[peri]['parameters']
            yaml_stream = open(parameters[peri]['dir'] + 'config.yml', 'w')
            yaml.dump(self.config, yaml_stream)
            yaml_stream.close()
            os.system(self.config['paths']['work_directory'] + 'run ' + parameters[peri]['dir']  + 'config.yml ')


class Calculator:
    def __init__(self):
        self.root_directory = os.path.realpath(__file__)
        self.root_directory = self.root_directory[:self.root_directory.rindex('/')] + '/'
        self.data_directory = self.root_directory + 'data/'
        self.existing_data = list()
        self.indexes = list()
        for name in os.listdir(self.data_directory):
            if not os.path.isfile(self.data_directory + name + '/config.yml'):
                shutil.rmtree(self.data_directory + name + '/')
                continue
            yaml_stream = open(self.data_directory + name + '/config.yml', 'r')
            config = yaml.load(yaml_stream)
            yaml_stream.close()
            self.existing_data.append(dict())
            self.existing_data[-1]['parameters'] = copy.deepcopy(config['parameters'])
            self.existing_data[-1]['number'] = int(name)
            self.indexes.append(int(name))

        self.indexes.sort()
        self.parameters_list = list()
        self.parameters_list.append({'name': 'kapa',
                                     'description': 'nonlinearity parameter'})
        self.parameters_list.append({'name': 'D',
                                     'description': 'noise width'})
        self.parameters_list.append({'name': 'Gpump',
                                     'description': 'relaxation of the pump mode'})
        self.parameters_list.append({'name': 'Gtwo',
                                     'description': 'relaxation of the signal and idler modes'})
        self.parameters_list.append({'name': 'nT',
                                     'description': 'temperature on signal and idler frequences'})
        self.parameters_list.append({'name': 'alpha',
                                     'description': 'pump amplitude'})
        self.parameters_list.append({'name': 'dt',
                                     'description': 'time step'})
        self.parameters_list.append({'name': 'Nsteps',
                                     'description': 'number of time steps'})
        self.parameters_list.append({'name': 'Ntr',
                                     'description': 'number of trajectories'})
        self.parameters_list.append({'name': 'Nout',
                                     'description': 'number of outputs calculations'})

    def add(self, parameters):
        def get_number_index(calc):
            nonlocal number_index
            nonlocal number_list_index
            if len(calc.indexes) == 0:
                number_index += 1
                return number_index - 1
            while True:
                if number_index != calc.indexes[number_list_index]:
                    number_index += 1
                    return number_index - 1
                else:
                    number_index += 1
                    if number_list_index != len(calc.indexes) - 1:
                        number_list_index += 1

        def check_current_parameters(calc):
            nonlocal current_parameters
            nonlocal parameters_to_calculate
            for parameter in calc.parameters_list:
                if not parameter['name'] in current_parameters:
                    raise Exception('Bad parametrs input: no information for ' + parameter['name'])
            new_calc = True

            for element in calc.existing_data:
                new_calc = False
                for key,value in element['parameters'].items():
                    if value != current_parameters[key]:
                        new_calc = True
                        break
                if not new_calc:
                    print(element['number'])
                    break
            if not new_calc:
                return
            parameters_to_calculate.append(dict())
            parameters_to_calculate[-1]['parameters'] = copy.deepcopy(current_parameters)
            parameters_to_calculate[-1]['dir'] = calc.data_directory + str(get_number_index(calc)) + '/'
            os.makedirs(parameters_to_calculate[-1]['dir'])
            yaml_stream = open(parameters_to_calculate[-1]['dir'] + 'config.yml', 'w')
            yaml.dump(parameters_to_calculate[-1], yaml_stream)
            yaml_stream.close()


        def parameters_iterate(name_index, par, calc):
            nonlocal current_parameters
            if name_index == len(par):
                check_current_parameters(calc)
            else:
                for value in par[name_index]:
                    if name_index == 0:
                        current_parameters = dict()
                    current_parameters.update(value)
                    parameters_iterate(name_index + 1, par, calc)

        current_parameters = dict()
        parameters_to_calculate = list()
        number_index = 0
        number_list_index = 0
        parameters_iterate(0, parameters, self)
        for element in  parameters_to_calculate:
            folder_name = os.getenv("HOME") + '/.runner/tasks/' + str(uuid.uuid4())
            os.mkdir(folder_name)
            parametr_file = open(folder_name + '/run_script','w')
            parametr_file.write(os.path.realpath(__file__))
            parametr_file.close()
            yaml_stream = open(folder_name + '/config.yml', 'w')
            yaml.dump(element, yaml_stream)
            yaml_stream.close()
            parametr_file = open(folder_name + '/ready', 'w')
            parametr_file.close()

    def ShowParametersList(self):
        for item in self.parameters_list:
            print(item['name'] + ' - ' + item['description'])

    def ShowOutputsList(self):
        print('Not made yet')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Get folder to proceed')
    parser.add_argument('folder', nargs='+', help='folder')
    args = parser.parse_args()
    yaml_stream = open(args.folder[0] + '/config.yml', 'r')
    parameters = list()
    parameters.append(yaml.load(yaml_stream))
    yaml_stream.close()
    n = Node()
    n.calculate(parameters,1,0,1)
    if os.path.isfile(parameters[0]['dir'] + 'output.nc'):
        shutil.rmtree(args.folder[0])