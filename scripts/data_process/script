#!/usr/bin/env python3

from netCDF4 import Dataset
import numpy as np
import math
import matplotlib.pyplot as plt
import argparse
import yaml

def En(a1, a2, I1, I2, a1k_a2, a1_a2, a1_a1, a2_a2):
    Gam = np.zeros((4, 4))
    Gam[0, 0] = a1_a1.real + I1 + 0.5 + 2 * a1.real * a1.real
    Gam[0, 1] = a1_a1.imag + 2 * a1.real * a1.imag
    Gam[0, 2] = a1k_a2.real + a1_a2.real + 2 * a1.real * a2.real
    Gam[0, 3] = a1k_a2.imag + a1_a2.imag + 2 * a1.real * a2.imag
    Gam[1, 0] = a1_a1.imag + 2 * a1.imag * a1.real
    Gam[1, 1] = -a1_a1.real + I1 + 0.5 + 2 * a1.imag * a1.imag
    Gam[1, 2] = a1_a2.imag - a1k_a2.imag + 2 * a1.imag * a2.real
    Gam[1, 3] = a1k_a2.real - a1_a2.real + 2 * a1.imag * a2.imag
    Gam[2, 0] = a1k_a2.real + a1_a2.real + 2 * a2.real * a1.real
    Gam[2, 1] = a1_a2.imag + a1k_a2.imag + 2 * a2.real * a1.imag
    Gam[2, 2] = a2_a2.real + I2 + 0.5 + 2 * a2.real * a2.real
    Gam[2, 3] = a2_a2.imag + 2 * a2.real * a2.imag
    Gam[3, 0] = - a1k_a2.imag + a1_a2.imag + 2 * a2.imag * a1.real
    Gam[3, 1] = a1k_a2.real - a1_a2.real + 2 * a2.imag * a1.imag
    Gam[3, 2] = a2_a2.imag + 2 * a2.imag * a2.real
    Gam[3, 3] = -a2_a2.real + I2 + 0.5 + 2 * a2.imag * a2.imag

    A = np.zeros((2, 2))
    B = np.zeros((2, 2))
    C = np.zeros((2, 2))

    A[0, 0] = Gam[0, 0]
    A[0, 1] = Gam[0, 1]
    A[1, 0] = Gam[1, 0]
    A[1, 1] = Gam[1, 1]

    B[0, 0] = Gam[2, 2]
    B[0, 1] = Gam[2, 3]
    B[1, 0] = Gam[3, 2]
    B[1, 1] = Gam[3, 3]

    C[0, 0] = Gam[0, 2]
    C[0, 1] = Gam[0, 3]
    C[1, 0] = Gam[1, 2]
    C[1, 1] = Gam[1, 3]

    sigm = np.linalg.det(A) + np.linalg.det(B) - 2 * np.linalg.det(C)
    gam = np.linalg.det(Gam)
    Neg = -math.log(2 * math.sqrt(0.5 * (sigm - math.sqrt(sigm * sigm - 4 * gam))), 2)
    if Neg < 0:
        Neg = 0
    return Neg

parser = argparse.ArgumentParser(description='Output parameters')
parser.add_argument('-dn','--data_number', nargs=1, type=int, help='number of data set')
args = parser.parse_args()

yaml_stream = open("data/"+str(args.data_number[0]) +"/config.yml", 'r')
config = yaml.load(yaml_stream)
yaml_stream.close()

for key,value in config['parameters'].items():
    print(str(key) + ': ' + str(value))

rootgrp = Dataset("data/"+str(args.data_number[0]) + "/output.nc", "r", format="NETCDF4")
# print( rootgrp.variables)
aS = np.array(rootgrp.variables['as_re']) + 1j * np.array(rootgrp.variables['as_im'])
aI = np.array(rootgrp.variables['ai_re']) + 1j * np.array(rootgrp.variables['ai_im'])
aSaS = np.array(rootgrp.variables['asas_re']) + 1j * np.array(rootgrp.variables['asas_im'])
aIaI = np.array(rootgrp.variables['aiai_re']) + 1j * np.array(rootgrp.variables['aiai_im'])
aSaI = np.array(rootgrp.variables['asai_re']) + 1j * np.array(rootgrp.variables['asai_im'])
akSaI = np.array(rootgrp.variables['asaki_re']) - 1j * np.array(rootgrp.variables['asaki_im'])
IS = np.array(rootgrp.variables['Isignal'])
II = np.array(rootgrp.variables['Iidle'])
IP = np.array(rootgrp.variables['Ipump'])
En_gaus = list(map(En, aS.tolist(), aI.tolist(), IS.tolist(), II.tolist(), akSaI.tolist(), aSaI.tolist(), aSaS.tolist(),
                   aIaI.tolist()))
En_full = np.array(rootgrp.variables['En'])
t = np.array(rootgrp.variables['Time'])
rootgrp.close()
# plt.plot(t, IP/np.max(IP),'b')
# plt.plot(t, IS/np.max(IS),'r')
# plt.plot(t, II/np.max(II),'g')
# plt.plot(t, En/np.max(En),'k')
# plt.plot(t, En_gaus/np.max(En_gaus),'--k')
fig = plt.figure(1)
plt.plot(t, IS, 'r')
plt.plot(t, II, 'g')
plt.plot(t, IP, 'b')
fig = plt.figure(2)
plt.plot(t, En_gaus, 'b')
plt.plot(t, En_full, 'r')

plt.show()
